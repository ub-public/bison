import { createStore } from 'vuex'
import helper from './helper'

const store = createStore({
  state: {
    journals: [],
    state: 'INITIAL',
    title: '',
    abstract: '',
    references: '',
    pre_language: null, // detected language used as pre-selected filter
    pre_subject: null, // detected subject
    error: '',
    result_view_type: 'list',
    extension: {
      additional_text: null,
      badge_text_blue: null,
      badge_text_green: null,
      badge_text_red: null,
      badge_text_yellow: null,
      badge_tooltip_blue: null,
      badge_tooltip_green: null,
      badge_tooltip_red: null,
      badge_tooltip_yellow: null,
      contact_mail: null,
      contact_linktext: null,
      institution_logo: null,
      is_set: false,
      licensed_journals: [],
      licenses: [],
      mirror_handling: 'IGNORE',
      mirror_jouranls: [],
      price_limit: Infinity,
      support_linktext: null,
      support_url: null
    },
    filters: {
      copyright_author_retains: false,
      publication_time: 0,
      subject: '',
      apc: 0,
      keywords: [],
      language: '',
      visible: false
    }
  },
  getters: {
    apcs_max (state) {
      return Math.ceil(Math.max(...state.journals.map((e) => e.has_apc ? e.apc_max.euro || e.apc_max.price : 0), 0) / 100) * 100
    },
    publication_time_max (state) {
      return Math.max(...state.journals.map((e) => e.publication_time_weeks), 0)
    },
    reset_filter_values (_, getters) {
      return {
        copyright_author_retains: false,
        publication_time: getters.publication_time_max,
        subject: '',
        language: '',
        apc: getters.apcs_max,
        keywords: [],
        visible: false
      }
    }
  },
  mutations: {
    SET_JOURNALS (state, journals) {
      state.journals = journals
      if (state.extension.is_set && state.extension !== undefined) {
        for (const journal of journals) {
          journal.mirror_journal = helper.methods.is_mirror_journal(journal, state.extension.mirror_journals)
          journal.licensed_journal = helper.methods.is_licensed_journal(journal, state.extension.licensed_journals)
          journal.blue_badge = helper.methods.gets_blue_badge(journal)
          journal.yellow_badge = helper.methods.gets_yellow_badge(journal, state.extension)
          journal.green_badge = helper.methods.gets_green_badge(journal)
          journal.red_badge = helper.methods.gets_red_badge(journal, state.extension)
        }
      }
    },
    SET_PRE_LANGUAGE (state, language) {
      state.pre_language = language
    },
    SET_PRE_SUBJECT (state, subject) {
      state.pre_subject = subject
    },
    SET_TITLE (state, title) {
      state.title = title
    },
    SET_ABSTRACT (state, abstract) {
      state.abstract = abstract
    },
    SET_REFERENCES (state, references) {
      state.references = references
    },
    RESET_FORM (state) {
      state.title = ''
      state.abstract = ''
      state.references = ''
      state.state = 'INITIAL'
    },
    SET_STATE (state, s) {
      state.state = s
    },
    SET_ERROR (state, error) {
      state.error = error
    },
    SET_FILTER_RETAINS_COPYRIGHT (state, checked) {
      state.filters.copyright_author_retains = checked
    },
    SET_FILTER_PUBLICATION_TIME (state, value) {
      state.filters.publication_time = value
    },
    SET_FILTER_SUBJECT (state, subject) {
      state.filters.subject = subject
    },
    SET_FILTER_LANGUAGE (state, language) {
      state.filters.language = language
    },
    SET_FILTER_APCS (state, apc) {
      state.filters.apc = apc
    },
    SET_FILTER_KEYWORDS (state, keywords) {
      state.filters.keywords = keywords
    },
    RESET_FILTERS (state, filters) {
      state.filters = filters
    },
    SET_FILTER_VISIBLE (state, visible) {
      state.filters.visible = visible
    },
    SET_RESULT_VIEW_TYPE (state, view) {
      state.result_view_type = view
    },
    SET_EXTENSION (state, extensionData) {
      state.extension = extensionData
      state.extension.is_set = true
    }
  },
  actions: {
    fetchExtension ({ commit, state }, extensionId) {
      fetch(helper.methods.get_api_url() + 'api/extension/' + extensionId)
        .then(function (response) {
          if (!response.ok) {
            throw Error(response.statusText)
          }
          return response
        }).then(response => response.json())
        .then(data => {
          commit('SET_EXTENSION', data)
        }).catch(error => {
          commit('SET_STATE', 'ERROR_EXTENSION')
          commit('SET_ERROR', error)
        })
    },
    journalQuery ({ commit, state, getters }) {
      if (state.state === 'LOADING') {
        return
      }
      if (state.title === '' && state.abstract === '' && state.references === '') {
        commit('SET_STATE', 'INITIAL')
        return
      }

      commit('SET_STATE', 'LOADING')
      fetch(helper.methods.get_api_url() + 'api/public/v1/search', {
        method: 'Post',
        body: JSON.stringify({
          title: state.title,
          abstract: state.abstract,
          references: state.references
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(function (response) {
        if (!response.ok) {
          throw Error(response.statusText)
        }
        return response
      }).then(response => response.json())
        .then(data => {
          commit('SET_JOURNALS', data.journals)
          commit('RESET_FILTERS', getters.reset_filter_values)
          commit('SET_PRE_LANGUAGE', data.language)
          commit('SET_PRE_SUBJECT', data.subject)
          if (data.language !== null) {
            commit('SET_FILTER_LANGUAGE', data.language)
          }
          commit('SET_STATE', 'RESULTS')
        }).catch(error => {
          commit('SET_STATE', 'ERROR')
          commit('SET_ERROR', error)
        })
    }
  }
})

export default store
