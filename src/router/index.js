import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/index.html.en',
    redirect: '/'
  },
  {
    path: '/index.html',
    redirect: '/'
  },
  {
    path: '/journal/:id',
    name: 'Journal',
    component: () => import('../views/Journal.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('../views/NotFound.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    return { top: 0 }
  },
  base: '/ub/publizieren/bison/',
  routes
})

export default router
