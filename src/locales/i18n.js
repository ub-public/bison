import { createI18n } from 'vue-i18n'

function loadLocaleMessages () {
  // from https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-vue-app-with-vue-i18n
  const locales = require.context('.', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
    }
  })
  return messages
}

const getLocale = () => {
  const cookieLanguage = document.cookie.split('; ').find((row) => row.startsWith('tuc_lang='))?.split('=')[1]
  if (cookieLanguage) {
    return cookieLanguage
  }
  return 'de'
}

export default createI18n({
  locale: getLocale(),
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadLocaleMessages()
})
