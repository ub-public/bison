const helper = {
  methods: {
    toId (input) {
      return input.replace(/\W/g, '')
    },
    journalToCsv (journal, rows) {
      let output = ''
      for (const row of rows) {
        if (output.length > 0) {
          output += ','
        }
        if (row === 'score') {
          output += journal.score.value
        } else if (row === 'apc_max') {
          output += journal.has_apc ? journal.apc_max.euro : '-'
        } else if (journal[row] === undefined || journal[row] === null) {
          output += '-'
        } else if (typeof journal[row] === 'object') {
          output += '"' + journal[row].join(',') + '"'
        } else {
          output += '"' + journal[row] + '"'
        }
      }
      return output
    },
    journalsToCsv () {
      const rows = [
        'title',
        'alternative_title',
        'score',
        'publisher_name',
        'pissn',
        'eissn',
        'publication_time_weeks',
        'doi_pid_scheme',
        'editorial_review_process',
        'copyright_author_retains',
        'plan_s_compliance',
        'languages',
        'apc_max'
      ]
      let csv = 'data:text/csv;charset=utf-8,'
      csv += rows.join(',') + '\n'
      csv += this.$store.state.journals.map(j => this.journalToCsv(j, rows))
        .join('\n')
      return encodeURI(csv)
    },
    score_precision (score) {
      return Math.round(score * 100) + '%'
    },
    bool_to_unicode (value) {
      return value === true ? '✓' : '✗'
    },
    array_to_string (array) {
      return array.join(', ')
    },
    localize_date (dateStr) {
      const date = Date.parse(dateStr)
      if (isNaN(date)) {
        return '-'
      }
      if (['de', 'en'].includes(navigator.language.split('-')[0])) {
        return new Intl.DateTimeFormat(navigator.language).format(date)
      }
      return new Intl.DateTimeFormat('en').format(date)
    },
    lang_code_detailed (isoCode) {
      const languages = {
        aa: 'Afar',
        ab: 'Abkhazian',
        ae: 'Avestan',
        af: 'Afrikaans',
        ak: 'Akan',
        am: 'Amharic',
        an: 'Aragonese',
        ar: 'Arabic',
        as: 'Assamese',
        av: 'Avar',
        ay: 'Aymara',
        az: 'Azerbaijani',
        ba: 'Bashkir',
        be: 'Belarusian',
        bg: 'Bulgarian',
        bh: 'Bihari',
        bi: 'Bislama',
        bm: 'Bambara',
        bn: 'Bengali',
        bo: 'Tibetan',
        br: 'Breton',
        bs: 'Bosnian',
        ca: 'Catalan',
        ce: 'Chechen',
        ch: 'Chamorro',
        co: 'Corsican',
        cr: 'Cree',
        cs: 'Czech',
        cu: 'Old Church Slavonic / Old Bulgarian',
        cv: 'Chuvash',
        cy: 'Welsh',
        da: 'Danish',
        de: 'German',
        dv: 'Divehi',
        dz: 'Dzongkha',
        ee: 'Ewe',
        el: 'Greek',
        en: 'English',
        eo: 'Esperanto',
        es: 'Spanish',
        et: 'Estonian',
        eu: 'Basque',
        fa: 'Persian',
        ff: 'Peul',
        fi: 'Finnish',
        fj: 'Fijian',
        fo: 'Faroese',
        fr: 'French',
        fy: 'West Frisian',
        ga: 'Irish',
        gd: 'Scottish Gaelic',
        gl: 'Galician',
        gn: 'Guarani',
        gu: 'Gujarati',
        gv: 'Manx',
        ha: 'Hausa',
        he: 'Hebrew',
        hi: 'Hindi',
        ho: 'Hiri Motu',
        hr: 'Croatian',
        ht: 'Haitian',
        hu: 'Hungarian',
        hy: 'Armenian',
        hz: 'Herero',
        ia: 'Interlingua',
        id: 'Indonesian',
        ie: 'Interlingue',
        ig: 'Igbo',
        ii: 'Sichuan Yi',
        ik: 'Inupiak',
        io: 'Ido',
        is: 'Icelandic',
        it: 'Italian',
        iu: 'Inuktitut',
        ja: 'Japanese',
        jv: 'Javanese',
        ka: 'Georgian',
        kg: 'Kongo',
        ki: 'Kikuyu',
        kj: 'Kuanyama',
        kk: 'Kazakh',
        kl: 'Greenlandic',
        km: 'Cambodian',
        kn: 'Kannada',
        ko: 'Korean',
        kr: 'Kanuri',
        ks: 'Kashmiri',
        ku: 'Kurdish',
        kv: 'Komi',
        kw: 'Cornish',
        ky: 'Kirghiz',
        la: 'Latin',
        lb: 'Luxembourgish',
        lg: 'Ganda',
        li: 'Limburgian',
        ln: 'Lingala',
        lo: 'Laotian',
        lt: 'Lithuanian',
        lv: 'Latvian',
        mg: 'Malagasy',
        mh: 'Marshallese',
        mi: 'Maori',
        mk: 'Macedonian',
        ml: 'Malayalam',
        mn: 'Mongolian',
        mo: 'Moldovan',
        mr: 'Marathi',
        ms: 'Malay',
        mt: 'Maltese',
        my: 'Burmese',
        na: 'Nauruan',
        nb: 'Norwegian Bokmål',
        nd: 'North Ndebele',
        ne: 'Nepali',
        ng: 'Ndonga',
        nl: 'Dutch',
        nn: 'Norwegian Nynorsk',
        no: 'Norwegian',
        nr: 'South Ndebele',
        nv: 'Navajo',
        ny: 'Chichewa',
        oc: 'Occitan',
        oj: 'Ojibwa',
        om: 'Oromo',
        or: 'Oriya',
        os: 'Ossetian / Ossetic',
        pa: 'Panjabi / Punjabi',
        pi: 'Pali',
        pl: 'Polish',
        ps: 'Pashto',
        pt: 'Portuguese',
        qu: 'Quechua',
        rm: 'Raeto Romance',
        rn: 'Kirundi',
        ro: 'Romanian',
        ru: 'Russian',
        rw: 'Rwandi',
        sa: 'Sanskrit',
        sc: 'Sardinian',
        sd: 'Sindhi',
        se: 'Northern Sami',
        sg: 'Sango',
        sh: 'Serbo-Croatian',
        si: 'Sinhalese',
        sk: 'Slovak',
        sl: 'Slovenian',
        sm: 'Samoan',
        sn: 'Shona',
        so: 'Somalia',
        sq: 'Albanian',
        sr: 'Serbian',
        ss: 'Swati',
        st: 'Southern Sotho',
        su: 'Sundanese',
        sv: 'Swedish',
        sw: 'Swahili',
        ta: 'Tamil',
        te: 'Telugu',
        tg: 'Tajik',
        th: 'Thai',
        ti: 'Tigrinya',
        tk: 'Turkmen',
        tl: 'Tagalog / Filipino',
        tn: 'Tswana',
        to: 'Tonga',
        tr: 'Turkish',
        ts: 'Tsonga',
        tt: 'Tatar',
        tw: 'Twi',
        ty: 'Tahitian',
        ug: 'Uyghur',
        uk: 'Ukrainian',
        ur: 'Urdu',
        uz: 'Uzbek',
        ve: 'Venda',
        vi: 'Vietnamese',
        vo: 'Volapük',
        wa: 'Walloon',
        wo: 'Wolof',
        xh: 'Xhosa',
        yi: 'Yiddish',
        yo: 'Yoruba',
        za: 'Zhuang',
        zh: 'Chinese',
        zu: 'Zulu'
      }
      if (typeof isoCode !== 'string') {
        return isoCode
      }
      isoCode = isoCode.toLowerCase()
      return isoCode in languages ? languages[isoCode] : isoCode
    },
    get_api_url () {
      if (this.is_extension_env()) {
        return process.env.VUE_APP_API_PATH || process.env.BASE_URL.replace('/extension', '')
      }
      return process.env.VUE_APP_API_PATH || process.env.BASE_URL
    },
    is_extension_env () {
      return process.env.VUE_APP_EXTENSION === '1'
    },
    gets_yellow_badge (journal, extension) {
      // at least one allowed license, 0 < apcs <= extension price limit, no mirror journal and no licensed journal
      const apcInRange = journal.has_apc && journal.apc_max.euro <= extension.price_limit
      const allowedLicense = journal.licenses.some(
        l => extension.licenses.some(
          l2 => JSON.stringify(l) === JSON.stringify(l2)))

      if (extension.mirror_handling === 'LABEL') {
        return apcInRange && allowedLicense && !journal.mirror_journal && !journal.licensed_journal
      }
      return apcInRange && allowedLicense && !journal.licensed_journal
    },
    gets_red_badge (journal, extension) {
      // none of the allowed licenses (intersection == 0), APCs too high or mirror journal
      const licensesOverlap = journal.licenses.some(l => extension.licenses.some(l2 => JSON.stringify(l) === JSON.stringify(l2)))
      const apcTooHigh = !journal.has_apc || journal.apc_max.euro > extension.price_limit
      if (extension.mirror_handling === 'LABEL') {
        return (!licensesOverlap && apcTooHigh) || !journal.mirror_journal
      }
      return !licensesOverlap && apcTooHigh
    },
    gets_green_badge (journal) {
      return !journal.has_apc
    },
    gets_blue_badge (journal) {
      return journal.licensed_journal
    },
    is_mirror_journal (journal, mirrorJournals) {
      return mirrorJournals.some(issn => issn.includes(journal.pissn) || issn.includes(journal.eissn))
    },
    is_licensed_journal (journal, licensedJournals) {
      return licensedJournals.some(issn => issn.includes(journal.pissn) || issn.includes(journal.eissn))
    },
    in_iframe () {
      try {
        return window.self !== window.top
      } catch (e) {
        return true
      }
    }
  }
}

export default helper
