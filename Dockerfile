FROM node:lts-alpine

ENV NODE_ENV=docker
ENV VUE_APP_API_APTH='https://service.tib.eu/bison/'
WORKDIR /app
RUN npm install -g http-server
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

EXPOSE 8080
CMD [ "http-server", "dist" ]