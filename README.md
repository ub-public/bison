# Buffalo

Buffalo is the front end for the [B!SON open access journal recommender](https://projects.tib.eu/bison).
The project is written in Vue.js and you can try out at [here](https://service.tib.eu/bison).

## Getting started
To run this project, first clone the repo.
Then run the following the commands to install the dependencies and lauch the site with hot-reloads.
The URL should appear in your terminal. 
```
npm install
npm run serve
```

Please note, that this only launches the front end. The back end is expected to run at port 3000 during development. For the back end, check out yak.


### Deployment
To compile and minify the files run:
```
npm run build
```

There is also a Dockerfile for building the code and running a demo server (without the API).
The environment variable VUE_APP_API_PATH can be set to run the front end on a different server/URL than the back end.


## Architecture
Buffalo uses Vue.js 3 with Vuex, Vue-router, Vue-I18n and Bootstrap for styling.

The structure is explained in the following graphic:

```
src
├── App.vue
├── assets
├── components   → smaller components like search forms
├── helper.js    → a set of helper methods as mixin
├── locales      → everything regarding i18n
├── main.js
├── router       → the routes and a mixin to set the page title
├── store.js     → the Vuex store
└── views        → the main pages, i.e. Home, About,...
```

## Contributing
Contributions are more than welcome! Please use the usual workflow of forking the repo, creating a branch and making a merge request.

## License
All of the code in this repository is licensed under the AGPL license.
